FROM golang:1.15

RUN apt-get update
RUN mkdir -p /go/src/app

ADD . /go/src/app

WORKDIR /go/src/app

RUN GO111MODULE=on \
    GOARCH=amd64 \
    GOOS=linux \
    CGO_ENABLED=0 \
    go build -o ./build/app -mod=vendor /go/src/app/cmd/main.go

