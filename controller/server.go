package controller

import (
	"github.com/foolin/gin-template"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"net/http"
)

func NewGin() *gin.Engine {
	g := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = []string{"authorization", "origin", "content-type", "accept", "x-trace-id", "user-language"}
	g.Use(cors.New(config))
	g.HTMLRender = gintemplate.Default()
	g.MaxMultipartMemory = 2048 << 20
	return g
}

type Server struct {
	db       *pg.DB
	logger   *zerolog.Logger
	engine   *gin.Engine
	apiGroup *gin.RouterGroup
}

func NewServer(
	db *pg.DB,
	logger *zerolog.Logger,
	engine *gin.Engine,
) *Server {
	return &Server{
		db:       db,
		logger:   logger,
		engine:   engine,
		apiGroup: engine.Group("/"),
	}
}

func (s *Server) Run() error {
	serv := &http.Server{
		Addr:    ":8080",
		Handler: s.engine,
	}
	return serv.ListenAndServe()
}

func (s *Server) ApiGroup() *gin.RouterGroup {
	return s.apiGroup
}
