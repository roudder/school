package main

import (
	"git.everytale.ru/backend/encoder/controller"
	"git.everytale.ru/backend/encoder/controller/photos"
	"git.everytale.ru/backend/encoder/util/db"
	"git.everytale.ru/backend/encoder/util/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"go.uber.org/dig"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	container := BuildContainer()
	err := container.Invoke(func(
		logger *zerolog.Logger,
		dbCon *pg.DB,
		gin *gin.Engine,
		server *controller.Server,
		photos *photos.ApiHandler,
	) {
		photos.HandleRoutes()
		go func() {
			if err := server.Run(); err != nil && err != http.ErrServerClosed {
				logger.Fatal().Err(err).Msg("error starting server")
			}
		}()
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
		<-quit
	})
	panic(err)
}

func BuildContainer() (container *dig.Container) {
	var err error
	container = dig.New()
	if err = container.Provide(logger.NewLogger); err != nil {
		panic(err)
	}
	if err = container.Provide(db.NewDbLogger); err != nil {
		panic(err)
	}
	if err = container.Provide(db.NewDb); err != nil {
		panic(err)
	}
	if err = container.Provide(controller.NewGin); err != nil {
		panic(err)
	}
	if err = container.Provide(controller.NewServer); err != nil {
		panic(err)
	}
	if err = container.Provide(photos.NewApiHandler); err != nil {
		panic(err)
	}
	return
}
