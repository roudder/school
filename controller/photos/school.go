package photos

import (
	"bytes"
	"git.everytale.ru/backend/encoder/model"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"net/url"
	"strconv"
)

func (h *ApiHandler) getSchoolById(c *gin.Context) {
	id := c.Param("id")
	m := &model.School{}
	err := h.db.Model(m).
		Where("id = ?", id).Select()
	if err != nil {
		h.logger.Warn().Caller().Err(err).Msg("db get school troubles")
	}
	c.JSON(http.StatusOK, m)
}

func (h *ApiHandler) searchSchoolAddress(c *gin.Context) {
	address := c.Query("address")
	decodedAddress, err := url.QueryUnescape(address)
	if err != nil {
		h.logger.Warn().Caller().Err(err).Msg("can't decode it")
		return
	}
	m := []model.School{}
	//err = h.db.Model(&m).Where("to_tsvector(address) @@ to_tsquery(?)", decodedAddress).Select()
	err = h.db.Model(&m).Where("address  ILIKE '%'||?||'%'", decodedAddress).Select()
	if err != nil {
		h.logger.Warn().Caller().Err(err).Msg("can't get school in db")
		return
	}
	idAddress := map[int]string{}
	for _, v := range m {
		idAddress[v.Id] = v.Address
	}
	c.JSON(http.StatusOK, idAddress)
}

func (h *ApiHandler) postReport(c *gin.Context) {
	schoolId, ok := c.GetPostForm("school_id")
	if !ok {
		h.logger.Warn().Caller().Msg("school_id is empty")
		return
	}
	id, err := strconv.Atoi(schoolId)
	if err != nil {
		h.logger.Warn().Caller().Err(err).Msg("wrong conv.")
		return
	}
	comment, _ := c.GetPostForm("comment")

	//multi photo
	multi, err := c.MultipartForm()
	if err != nil {
		h.logger.Warn().Caller().Err(err).Msg("multi upload troubles")
		return
	}
	files := multi.File["pictures"]
	sPhotos := []model.SchoolPhotos{}
	for _, v := range files {
		file := v
		f, err := file.Open()
		if err != nil {
			h.logger.Warn().Caller().Err(err).Msg("can't open file")
			return
		}
		defer f.Close()
		buf := bytes.NewBuffer(nil)
		io.Copy(buf, f)
		sPhotos = append(sPhotos, model.SchoolPhotos{SchoolId: id, Photo: buf.Bytes(), Comment: comment})
	}
	_, err = h.db.Model(&sPhotos).WherePK().Insert()
	if err != nil {
		h.logger.Warn().Caller().Err(err).Msg("update photo trouble")
		return
	}
	c.JSON(http.StatusOK, "Отчет успешно сохранен")
}
