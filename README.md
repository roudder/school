# School

School is a project which helps store reports with photos, comments in db.

## Installation

Clone project

Navigate to project folder

```bash
docker-compose up -d
```
Navigate to localhost:8080

## DB

DB: school

User: school

Password: YWdEn^IcfJU6

Db data will store in project dir. (out of containers) in path pgdata/schooldb