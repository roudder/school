package photos

import (
	"git.everytale.ru/backend/encoder/controller"
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"net/http"
)

type ApiHandler struct {
	logger *zerolog.Logger
	server *controller.Server
	engine *gin.Engine
	db     *pg.DB
}

func NewApiHandler(
	logger *zerolog.Logger,
	server *controller.Server,
	engine *gin.Engine,
	db *pg.DB,
) *ApiHandler {
	return &ApiHandler{
		logger: logger,
		server: server,
		engine: engine,
		db:     db,
	}
}

func (h *ApiHandler) HandleRoutes() {
	//h.engine.GET("school/:id", h.getSchoolById)
	h.engine.GET("search-school-address", h.searchSchoolAddress)
	h.engine.POST("report", h.postReport)
	h.engine.GET("", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "index.html", gin.H{"title": "hello"})
	})
}
