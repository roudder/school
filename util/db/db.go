package db

import (
	"github.com/go-pg/migrations"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"strings"
)

func NewDb(
	dbLogger *Logger,
) *pg.DB {

	db := pg.Connect(&pg.Options{
		Addr:     "postgre:5432",
		User:     "school",
		Password: "YWdEn^IcfJU6",
		Database: "school",
		PoolSize: 1000,
	})

	var n int
	_, err := db.QueryOne(pg.Scan(&n), "SELECT 1")
	if err != nil {
		log.Fatal().Err(err)
	}

	applyMigrationsDB(db, dbLogger.logger)

	return db
}

func NewDbLogger(
	logger *zerolog.Logger,
) *Logger {
	return &Logger{
		logger: logger,
	}
}

type Logger struct {
	logger *zerolog.Logger
}

func (d *Logger) BeforeQuery(q *pg.QueryEvent) {}
func (d *Logger) AfterQuery(q *pg.QueryEvent) {
	query, _ := q.FormattedQuery()
	d.logger.Debug().Str("query", strings.Replace(strings.Replace(query, "\n", " ", -1), "\t", " ", -1)).Send()
}

func applyMigrationsDB(db *pg.DB, logger *zerolog.Logger) {
	migrations.Run(db, "init")
	oldVersion, newVersion, err := migrations.Run(db, "up")
	logger.Info().Msg("run migrations")
	if err != nil {
		panic(err.Error())
	}
	if newVersion != oldVersion {
		logger.Debug().Msgf("migrated from version %d to %d\n", oldVersion, newVersion)
	} else {
		logger.Debug().Msgf("version is %d\n", oldVersion)
	}
	logger.Info().Msg("migrations done")
}
