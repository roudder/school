package logger

import (
	"github.com/rs/zerolog"
	"os"
	"time"
)

func NewLogger() *zerolog.Logger {
	output := zerolog.ConsoleWriter{
		Out:        os.Stdout,
		TimeFormat: time.RFC3339Nano,
		NoColor:    true,
	}
	log := zerolog.New(output).With().Timestamp().Caller().Logger()
	return &log
}
