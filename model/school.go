package model

type School struct {
	tableName struct{} `sql:"schools"`
	Id        int      `json:"id"`
	Name      string   `json:"name"`
	Address   string   `json:"address"`
}

type SchoolPhotos struct {
	tableName struct{} `sql:"school_photos"`
	Id        int      `json:"id"`
	SchoolId  int      `json:"school_id"`
	Comment   string   `json:"comment"`
	Photo     []byte
}
