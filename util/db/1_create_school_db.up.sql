create table schools
(
    id      serial
        constraint school_pk
            primary key,
    name    varchar(200),
    address varchar(300)
);

create table school_photos
(
    id        serial
        constraint school_photo_pk
            primary key,
    school_id integer
        constraint school_photos_school_id_fk
            references schools,
    photo     bytea,
    comment   varchar(500)
);

--test values
insert into schools
    (name, address)
values ('Сош 25', '238313, Калининградская область, Гурьевский район, пос. Низовье, ул.Калининградская, 1а'),
       ('Сош 20', '238311, Калининградская область, Гурьевский район, п. Большое Исаково, улица Анны Бариновой, д.1');



