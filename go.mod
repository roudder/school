module git.everytale.ru/backend/encoder

go 1.15

require (
	github.com/foolin/gin-template v0.0.0-20190415034731-41efedfb393b
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-pg/migrations v6.7.3+incompatible
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/rs/zerolog v1.19.0
	go.uber.org/dig v1.10.0
	mellium.im/sasl v0.2.1 // indirect
)
